package pw.tcrs.killgreg;

import java.util.List;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import pw.tcrs.tcrscore.sum.BaseItem;

public class ItemKillGreg extends BaseItem {

	public ItemKillGreg(int par1) {
		super(par1);
		this.setMaxDamage(0);
		this.setHasSubtypes(true);
	}
	
	@SideOnly(Side.CLIENT)
    private Icon[] iconItemKillGreg;
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIconFromDamage(int par1) {
		return super.getIconFromDamage(par1);
	}
 
	@Override
	public int getMetadata(int par1) {
		return par1;
	}
 
	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack) {
		//名前の登録
		return this.getUnlocalizedName() + "_" + par1ItemStack.getItemDamage();
	}
 
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List) {
		//メタデータでアイテムを追加する
		//par3List.add(new ItemStack(this, 1, <メタデータ>));
		par3List.add(new ItemStack(this, 1, 0));
		par3List.add(new ItemStack(this, 1, 1));
	}
        @Override
        @SideOnly(Side.CLIENT)
        public void registerIcons(IconRegister par1IconRegister) {
                //テクスチャのパス指定。
                //メタデータは0から2でつまり3未満
                //src/minecraft/assets/samplemod/items/itemsample_(メタデータ).png
        this.iconItemKillGreg = new Icon[3];
 
        for (int i = 0; i < this.iconItemKillGreg.length; ++i)
        {
            this.iconItemKillGreg[i] = par1IconRegister.registerIcon("killgreg:Items_" + i);
        }
    }

}
