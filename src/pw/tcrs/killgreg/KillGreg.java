/**
 * 
 */
package pw.tcrs.killgreg;

import ic2.api.item.Items;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.minecraft.block.Block;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;
import pw.tcrs.killgreg.Block.MetalFormer;
import pw.tcrs.killgreg.gui.GuiHamdlerMetalFormer;
import pw.tcrs.killgreg.tileentity.TileEntityMetalFormer;
import pw.tcrs.tcrscore.TcrsCore;
import pw.tcrs.tcrscore.sum.IMOD;
import asia.tcrs.mtc.api.MTCAPI;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLInterModComms;
import cpw.mods.fml.common.event.FMLInterModComms.IMCEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

/**
 * @author inaka
 * 
 */
@Mod(modid = "tcrs_KillGreg", name = "KillGreg", version = "Beta1.1", dependencies = "required-after:TcrsCore2.5;required-after:tcrs_materialconverter;required-after:IC2;required-after:MTC_APMachine;")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class KillGreg implements IMOD {

	@Mod.Instance("tcrs_KillGreg")
	public static KillGreg instance;

	public static Logger logger = Logger.getLogger("tcrs_KillGreg");

	public static Item KillGreg;
	public static Item RewAdvancedCircuit;

	public static Item StoneHammer;
	public static Item StoneSaw;
	public static Item ICHammerCraftKit;

	public static Block MetalFormerB;
	public static Block MetalFormerActive;

	public int KillGregItemID;
	public static int MetalFormerID;
	public static int MetalFormerActiveID;

	public static boolean easySteel;

	private static MTCAPI api;

	/*
	 * (non-Javadoc)
	 * 
	 * @see pw.tcrs.tcrscore.sum.IMOD#preload(cpw.mods.fml.common.event.
	 * FMLPreInitializationEvent)
	 */
	@Override
	@EventHandler
	public void preload(FMLPreInitializationEvent event) {
		Configuration cfg = new Configuration(
				TcrsCore.getconfigfile("KillGreg"));
		try {
			cfg.load();
			Property KillGregProp = cfg.getItem("ElectronicCircuit", 5000);
			// ElectronicCircuitProp.comment = "This comment is Item Property";
			Property MetalFormerProp = cfg.getBlock("Metal Former", 1860);
			Property MetalFormerActiveProp = cfg.getBlock(
					"Metal Former Active", 1861);
			KillGregItemID = KillGregProp.getInt();
			MetalFormerID = MetalFormerProp.getInt();
			MetalFormerActiveID = MetalFormerActiveProp.getInt();
		} catch (Exception e) {
			FMLLog.log(Level.SEVERE, e, "Error Message");
		} finally {
			cfg.save();
		}
		api = MTCAPI.instance();
		registerItem();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pw.tcrs.tcrscore.sum.IMOD#registerItem()
	 */
	@Override
	public void registerItem() {

		KillGreg = new Item(KillGregItemID)
				.setUnlocalizedName("killgreg:ItemKillGreg")
				.setTextureName("killgreg:ItemKillGreg")
				.setCreativeTab(MTCAPI.MTCAddonTab);
		RewAdvancedCircuit = new Item(KillGregItemID + 4)
				.setUnlocalizedName("killgreg:RewAdvancedCircuit")
				.setTextureName("killgreg:RewAdvancedCircuit")
				.setCreativeTab(MTCAPI.MTCAddonTab);
		GameRegistry.registerItem(KillGreg, "ItemKillGreg");
		StoneHammer = new ItemSword(KillGregItemID + 1, EnumToolMaterial.STONE)
				.setUnlocalizedName("killgreg:StoneHammer")
				.setTextureName("killgreg:StoneHammer")
				.setCreativeTab(MTCAPI.MTCAddonTab);
		StoneSaw = new ItemSword(KillGregItemID + 2, EnumToolMaterial.STONE)
				.setUnlocalizedName("killgreg:StoneSaw")
				.setTextureName("killgreg:StoneSaw")
				.setCreativeTab(MTCAPI.MTCAddonTab);
		ICHammerCraftKit = new Item(KillGregItemID + 3)
				.setUnlocalizedName("killgreg:ICHammerCraftKit")
				.setTextureName("killgreg:ICHammerCraftKit")
				.setCreativeTab(MTCAPI.MTCAddonTab);
		MetalFormerB = new MetalFormer(MetalFormerID, false)
				.setUnlocalizedName("killgreg:MetalFormer").setHardness(50.0F)
				.setResistance(2000.0F).setCreativeTab(MTCAPI.MTCAddonTab);
		MetalFormerActive = new MetalFormer(MetalFormerActiveID, true)
				.setUnlocalizedName("killgreg:MetalFormerActive")
				.setHardness(50.0F).setResistance(2000.0F);
		GameRegistry.registerBlock(MetalFormerB, "killgreg:MetalFormer");
		GameRegistry.registerBlock(MetalFormerActive,
				"killgreg:MetalFormerActive");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pw.tcrs.tcrscore.sum.IMOD#load(cpw.mods.fml.common.event.
	 * FMLInitializationEvent)
	 */
	@Override
	@EventHandler
	public void load(FMLInitializationEvent event) {

		LanguageRegistry.addName(new ItemStack(KillGreg),
				"Rew Electronic Circuit");
		LanguageRegistry.addName(new ItemStack(KillGreg),
				"Rew Advanced Circuit");
		LanguageRegistry.addName(MetalFormerB, "KGMetalFormer");
		LanguageRegistry.addName(ICHammerCraftKit, "ICHammerCraftKit");
		LanguageRegistry.addName(StoneHammer, "StoneHammer");
		LanguageRegistry.addName(StoneSaw, "StoneSaw");

		GameRegistry.registerTileEntity(TileEntityMetalFormer.class,
				"TileEntityMetalFormer");
		NetworkRegistry.instance().registerGuiHandler(this,
				new GuiHamdlerMetalFormer());

		GameRegistry.addRecipe(new ItemStack(KillGreg, 1, 0), new Object[] {
				"XXX", "YZY", "XXX", 'X', Items.getItem("copperCableItem"),
				'Y', Item.redstone, 'Z', Item.ingotIron });

		GameRegistry.addRecipe(new ItemStack(KillGreg, 1, 1),
				new Object[] { "YXY", "ZAZ", "YXY", 'X', Item.glowstone, 'A',
						Items.getItem("electronicCircuit"), 'Y', Item.redstone,
						'Z', new ItemStack(Item.dyePowder, 1, 4) });

		GameRegistry.addRecipe(new ItemStack(KillGreg, 1, 1), new Object[] {
				"YXY", "ZAZ", "YXY", 'X', Item.glowstone, 'A',
				new ItemStack(KillGreg, 1, 0), 'Y', Item.redstone, 'Z',
				new ItemStack(Item.dyePowder, 1, 4) });

		GameRegistry.addRecipe(new ItemStack(StoneHammer), new Object[] {
				"ZZ ", "ZZA", "ZZ ", 'A', new ItemStack(Item.stick), 'Z',
				new ItemStack(Block.cobblestone) });
		GameRegistry.addRecipe(new ItemStack(StoneSaw), new Object[] { "AAA",
				"ZZA", 'A', new ItemStack(Item.stick), 'Z',
				new ItemStack(Block.cobblestone) });
		GameRegistry.addRecipe(new ItemStack(ICHammerCraftKit), new Object[] {
				"ZZZ", "ZAZ", " A ", 'A', new ItemStack(Item.stick), 'Z',
				new ItemStack(Item.ingotIron) });
		GameRegistry.addRecipe(new ItemStack(MetalFormerB), new Object[] {
				"AZA", "A A", "AAA", 'A', new ItemStack(Block.cobblestone),
				'Z', new ItemStack(StoneHammer) });
		TcrsCore.getOreDicItem("dustSteel", TcrsCore.MODList.GregTech);
		ItemStack ingotSteel = TcrsCore.getOreDicItem("ingotSteel",
				TcrsCore.MODList.GregTech);
		logger.log(Level.ALL, "killgreg:" + ingotSteel.getUnlocalizedName());
		NBTTagCompound toSend;
		// easySteel=true;
		// GameRegistry.addRecipe(
		// new ShapelessOreRecipe(
		// dustSteel.splitStack(3),
		// new Object[]
		// {
		// "dustIron","dustIron","dustIron","dustCoal"
		// }));
		// //
		// toSend = TcrsCore.CreateFurnaceStyleRecipeData(dustSteel, ingotSteel,
		// true);
		// FMLInterModComms.sendMessage("MTC_APMachine", "addAPMachineRecipe",
		// toSend);
		//
		toSend = TcrsCore.CreateFurnaceStyleRecipeData(new ItemStack(
				ICHammerCraftKit), Items.getItem("ForgeHammer"), false);
		FMLInterModComms.sendMessage("MTC_APMachine", "addAPMachineRecipe",
				toSend);
		//
		toSend = TcrsCore.CreateFurnaceStyleRecipeData(new ItemStack(KillGreg),
				Items.getItem("electronicCircuit"), false);
		FMLInterModComms.sendMessage("MTC_APMachine", "addAPMachineRecipe",
				toSend);
		//
		toSend = TcrsCore.CreateFurnaceStyleRecipeData(new ItemStack(
				RewAdvancedCircuit), Items.getItem("advancedCircuit"), false);
		FMLInterModComms.sendMessage("MTC_APMachine", "addAPMachineRecipe",
				toSend);
		//

		KGMFItem.Load();

	}

	@EventHandler
	public void receiveIMC(IMCEvent event) {
		IMC_Receive_KGg.receiveIMC(event);
	}

}
