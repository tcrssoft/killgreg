package pw.tcrs.killgreg;

import java.util.logging.Level;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import cpw.mods.fml.common.event.FMLInterModComms.IMCEvent;
import cpw.mods.fml.common.event.FMLInterModComms.IMCMessage;

public class IMC_Receive_KGg {
	private IMC_Receive_KGg(){}
	 
	public static void receiveIMC(IMCEvent event)
	{
		for (IMCMessage message : event.getMessages())
		{
			if (message.key.equals("addAPMachineRecipe"))
			{
				receiveAddRecipe(event, message);
			}
			else
			{
				KillGreg.logger.warning("Received IMC message with unknown key : " + message.key);
			}
		}
	}
 
	private static void receiveAddRecipe(IMCEvent event, IMCMessage message)
	{
		boolean flag = false;
		if (message.isNBTMessage())
		{
			NBTTagCompound tag = message.getNBTValue();
			if (tag.hasKey("input") && tag.hasKey("output") && tag.hasKey("DamageSensitive"))
			{
				ItemStack input = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("input"));
				ItemStack output = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("output"));
				boolean sensitive = tag.getBoolean("DamageSensitive");
				KillGreg.logger.log(Level.INFO,"IMCCatch"+input.getUnlocalizedName());
				KillGreg.logger.log(Level.INFO,"IMCCatch"+output.getUnlocalizedName());
 
				if (input != null && output != null)
				{
					/*booleanの結果により、2通りの方法で精錬レシピを追加*/
					if (sensitive==true)
					{
						MetalFormerRecipes.smelting().addSmelting(input, output, 0.0F);	
					}
					else
					{
						ItemStack a = input;
						a.setItemDamage(32767);
						MetalFormerRecipes.smelting().addSmelting(a, output, 0.0F);
					}
					/*成功フラグ*/
					flag = true;
				}
			}
		}
		if (!flag)
		{
			KillGreg.logger.warning("Failed to register new MetalFormer recipe with IMCMassage from " + message.getSender());
		}
	}
}
