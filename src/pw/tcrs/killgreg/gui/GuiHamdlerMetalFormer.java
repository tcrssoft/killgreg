package pw.tcrs.killgreg.gui;

import pw.tcrs.killgreg.tileentity.ContainerMetalFormer;
import pw.tcrs.killgreg.tileentity.TileEntityMetalFormer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHamdlerMetalFormer implements IGuiHandler{

	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if(tileEntity instanceof TileEntityMetalFormer){
                return new ContainerMetalFormer(player.inventory, (TileEntityMetalFormer) tileEntity);
        }
        return null;
	}
	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
        if(tileEntity instanceof TileEntityMetalFormer){
                return new GuiMetalFormer(player.inventory, (TileEntityMetalFormer) tileEntity);
        }
        return null;
	}
}
