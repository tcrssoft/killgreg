package pw.tcrs.killgreg.gui;

import org.lwjgl.opengl.GL11;

import pw.tcrs.killgreg.tileentity.ContainerMetalFormer;
import pw.tcrs.killgreg.tileentity.TileEntityMetalFormer;

import asia.tcrs.mtc.APMachine.ContainerAPMachine;
import asia.tcrs.mtc.APMachine.TileEntityAPMachine;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

public class GuiMetalFormer extends GuiContainer{

	private ResourceLocation better = new ResourceLocation("textures/gui/container/furnace.png");

	
	private TileEntityMetalFormer furnaceInventory;

    public GuiMetalFormer(InventoryPlayer par1InventoryPlayer, TileEntityMetalFormer tileEntity)
    {
        super(new ContainerMetalFormer(par1InventoryPlayer, tileEntity));
        this.furnaceInventory = tileEntity;
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int par1, int par2)
    { 
    	String s = StatCollector.translateToLocal("MetalFormer");
        this.fontRenderer.drawString(s, this.xSize / 2 - this.fontRenderer.getStringWidth(s) / 2, 6, 4210752);
        this.fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 8, this.ySize - 96 + 2, 4210752);
    }

    /**
     * Draw the background layer for the GuiContainer (everything behind the items)
     */
    protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(better);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        int i1;

        if (this.furnaceInventory.isBurning())
        {
            i1 = this.furnaceInventory.getBurnTimeRemainingScaled(12);
            this.drawTexturedModalRect(k + 56, l + 36 + 12 - i1, 176, 12 - i1, 14, i1 + 2);
        }

        i1 = this.furnaceInventory.getCookProgressScaled(24);
        this.drawTexturedModalRect(k + 79, l + 34, 176, 14, i1 + 1, 16);
    }
	
}
