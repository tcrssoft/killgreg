package pw.tcrs.killgreg;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import asia.tcrs.mtc.renkin;
import asia.tcrs.mtc.api.MTCAPI;

import pw.tcrs.tcrscore.TcrsCore;
import pw.tcrs.tcrscore.sum.SUM_GameRegistry;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
public class KGMFItem {
	public static ItemStack IronPlate,GoldPlate,SteelPlate,BronzePlate,TinPlate,CopperPlate,IronStick,GoldStick;
	
	private static void GetItem()
	{
		IronPlate=TcrsCore.getOreDicItem("plateIron", TcrsCore.MODList.GregTech);
		GoldPlate=TcrsCore.getOreDicItem("plateGold", TcrsCore.MODList.GregTech);
		SteelPlate=TcrsCore.getOreDicItem("craftingPlateSteel", TcrsCore.MODList.GregTech);
		BronzePlate=TcrsCore.getOreDicItem("plateBronze", TcrsCore.MODList.GregTech);
		TinPlate=TcrsCore.getOreDicItem("plateTin", TcrsCore.MODList.GregTech);
		CopperPlate=TcrsCore.getOreDicItem("plateCopper", TcrsCore.MODList.GregTech);
		IronStick=MTCAPI.getItemStack("ironstick").splitStack(2);
		GoldStick=MTCAPI.getItemStack("goldstick").splitStack(2);
	}
	
	public static void Load()
	{
		GetItem();
		MetalFormerRecipes.smelting().addSmelting(new ItemStack(Item.ingotIron), IronPlate, 0F);
		MetalFormerRecipes.smelting().addSmelting(new ItemStack(Item.ingotGold), GoldPlate, 0F);
		TcrsCore.addOredicSmelting("ingotSteel", SteelPlate);
		TcrsCore.addOredicSmelting("ingotBronze", BronzePlate);
		TcrsCore.addOredicSmelting("ingotTin", TinPlate);
		TcrsCore.addOredicSmelting("ingotCopper", CopperPlate);
		TcrsCore.addOredicSmelting("plateIron", IronStick);
		TcrsCore.addOredicSmelting("plateGold", GoldStick);
	}
}
